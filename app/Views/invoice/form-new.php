<html>
  <head>
    <!-- <script type="text/javascript" src="/bootstrap/js/bootstrap.min.js"></script>
    <link href="/bootstrap/css/bootstrap.min.css" rel="stylesheet"> -->
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@3.4.1/dist/css/bootstrap.min.css" integrity="sha384-HSMxcRTRxnN+Bdg0JdbxYKrThecOKuH5zCYotlSAcp1+c8xmyTe9GYg1l9a69psu" crossorigin="anonymous">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@3.4.1/dist/css/bootstrap-theme.min.css" integrity="sha384-6pzBo3FDv/PJ8r2KRkGHifhEocL+1X2rVCTTkUfGk7/0pbek5mMa1upzvWbrUbOZ" crossorigin="anonymous">

    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.12.1/css/jquery.dataTables.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.12.1/css/dataTables.bootstrap.min.css">
  </head>
  <body>
    <?php echo view("template/navbar") ?>
    <div class="container">
      <div class="row">
        <?php $isEdit = false; if(isset($invoice)) { $isEdit = true; var_dump?>
        <h1>Edit Invoice - <?php echo $invoice->invoice_id; ?></h1>
        <?php } else { ?>
        <h1>New Invoice</h1>
        <?php } ?>
      </div>
    </div><!-- /.container -->
    <div class="container">
      <form class="form-horizontal" method="POST" action="<?php echo base_url("invoice/formp"); ?>">
        <div class="row">
          <div class="col-md-6">
            <?php if($isEdit) { ?><input type="hidden" class="form-control" id="invoiceIdInput" placeholder="Invoice ID" name="invoiceId" value="<?php echo $invoice->invoice_id; ?>" ><?php } ?>
            <div class="form-group">
              <label for="issueDateInput" class="col-md-3 control-label">Issue Date</label>
              <div class="col-md-9">
                <input type="date" class="form-control" id="issueDateInput" placeholder="Issue Date" name="issueDate" <?php if($isEdit) { echo "value='".$invoice->issue_date."'"; } ?> required>
              </div>
            </div>
            <div class="form-group">
              <label for="dueDateInput" class="col-md-3 control-label">Due Date</label>
              <div class="col-md-9">
                <input type="date" class="form-control" id="dueDateInput" placeholder="Due Date" name="dueDate" <?php if($isEdit) { echo "value='".$invoice->due_date."'"; } ?> required>
              </div>
            </div>
            <div class="form-group">
              <label for="subjectInput" class="col-md-3 control-label">Subject</label>
              <div class="col-md-9">
                <input type="text" class="form-control" id="subjectInput" placeholder="Subject" name="subject" <?php if($isEdit) { echo "value='".$invoice->subject."'"; } ?> required>
              </div>
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
              <label for="fromSubjectInput" class="col-md-3 control-label">From Subject</label>
              <div class="col-md-9">
                <input type="text" class="form-control" id="fromSubjectInput" placeholder="From Subject" name="fromSubject" <?php if($isEdit) { echo "value='".$invoice->from_subject."'"; } ?> required>
              </div>
            </div>
            <div class="form-group">
              <label for="fromAddressInput" class="col-md-3 control-label">From Address</label>
              <div class="col-md-9">
                <textarea class="form-control" rows="3" id="fromAddressInput" name="fromAddress" required><?php if($isEdit) { echo $invoice->from_address; } ?></textarea>
              </div>
            </div>
            <div class="form-group">
              <label for="forSubjectInput" class="col-md-3 control-label">For Subject</label>
              <div class="col-md-9">
                <select class="form-control" id="selectCustomer" required>
                  <option value="" for_subject="" for_address="">Select Customer</option>
                  <?php for ($i=0; $i < count($customer); $i++) { $c = $customer[$i]?>
                    <option value="<?php echo $c->id; ?>" for_subject="<?php echo $c->name; ?>" for_address="<?php echo $c->address ?>" <?php if($isEdit) { if($c->name === $invoice->for_subject) {echo "selected";} } ?> ><?php echo $c->name; ?></option>
                  <?php } ?>
                </select>
                <input type="hidden" class="form-control" id="forSubjectInput" placeholder="For Subject" name="forSubject" <?php if($isEdit) { echo "value='".$invoice->for_subject."'"; } ?>>
              </div>
            </div>
            <div class="form-group">
              <label for="forAddressInput" class="col-md-3 control-label">For Address</label>
              <div class="col-md-9">
                <textarea class="form-control" rows="3" id="forAddressInput" name="forAddress" readonly required></textarea>
              </div>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-1 pull-right">
            <button class="btn btn-primary btn-block" type="button" id="btnAdd">Add</button>
          </div>
        </div>
        <div class="row">&nbsp;</div>
        <div class="row">
          <div class="col-md-12">
            <table class="table table-striped table-bordered">
              <thead>
                <tr>
                  <td class="col-md-2">
                    Item Type
                  </td>
                  <td class="col-md-4">
                    Description
                  </td>
                  <td class="col-md-1">
                    Quantity
                  </td>
                  <td class="col-md-2">
                    Unit Price
                  </td>
                  <td class="col-md-2">
                    Amount
                  </td>
                  <td class="col-md-1">
                    Action
                  </td>
                </tr>
              </thead>
              <tbody id="itemBody">
                <?php 
                  if($isEdit){ 
                    for ($i=0; $i < count($invoice_detail); $i++) { $d = $invoice_detail[$i];
                ?>
                <tr>
                  <td>
                    <label class="sr-only">Item Type</label>
                    <select class="form-control selectItem" required>
                      <option value="" item_type="" item_desc="" item_price="0">Select Item</option>
                      <?php for ($j=0; $j < count($item); $j++) { $t = $item[$j]?>
                      <option value="<?php echo $t->id; ?>" item_type="<?php echo $t->type; ?>" item_desc="<?php echo $t->description; ?>" item_price="<?php echo $t->price; ?>" <?php if($t->description === $d->item_description) {echo "selected";} ?> ><?php echo $t->type."(".$t->description.")"; ?></option>
                      <?php } ?>
                    </select>
                    <input type="hidden" class="form-control inputType" placeholder="Type" name="type[]">
                  </td>
                  <td>
                    <label class="sr-only">Description</label>
                    <input type="text" class="form-control inputDesc" placeholder="Description" name="description[]" readonly required>
                  </td>
                  <td>
                    <label class="sr-only">Quantity</label>
                    <input type="number" class="form-control inputQty" placeholder="QTY" name="quantity[]" min=1 step=0.1 <?php if($isEdit) { echo "value='".$d->item_quantity."'"; } ?> required>
                  </td>
                  <td>
                    <label class="sr-only">Price</label>
                    <input type="text" class="form-control inputPrice" placeholder="Price" name="price[]" value=0 readonly required>
                  </td>
                  <td>
                    <label class="sr-only">Amount</label>
                    <input type="text" class="form-control inputAmount" placeholder="Amount" value=0 readonly required>
                  </td>
                  <td>
                    <button class="btn btn-danger btnRemove" type="button" >Remove</button>
                  </td>
                </tr>
                <?php 
                    }
                  }
                ?>
              </tbody>
            </table>
          </div>
        </div>
        <div class="row">&nbsp;</div>
        <div class="row">
          <div class="col-md-offset-8 col-md-4">
            <div class="form-group">
              <label class="col-md-3 control-label">Subtotal</label>
              <div class="col-md-9">
                <input type="number" class="form-control" placeholder="Subtotal" id="subtotalInput" value=0 readonly>
              </div>
            </div>
            <div class="form-group">
              <label for="taxInput" class="col-md-3 control-label">Tax</label>
              <div class="col-md-9">
                <input type="number" class="form-control" id="taxInput" placeholder="Tax" name="tax" <?php if($isEdit) { echo "value='".$invoice->tax."'"; } ?> required>
              </div>
            </div>
            <div class="form-group">
              <label class="col-md-3 control-label">Total</label>
              <div class="col-md-9">
                <input type="number" class="form-control" placeholder="Total" id="totalInput" value=0 readonly>
              </div>
            </div>
            <div class="form-group">
              <label for="paymentInput" class="col-md-3 control-label">Payment</label>
              <div class="col-md-9">
                <input type="number" class="form-control" id="paymentInput" placeholder="Payment" name="payment" <?php if($isEdit) { echo "value='".$invoice->payment."'"; } ?> required>
              </div>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-1 pull-right">
            <button class="btn btn-primary btn-block" id="btnSave" type="submit">Save</button>
          </div>
        </div>
      </form>
    </div><!-- /.container -->
  </body>
  <footer>
    <!-- JQuery -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <!-- Latest compiled and minified JavaScript -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@3.4.1/dist/js/bootstrap.min.js" integrity="sha384-aJ21OjlMXNL5UyIl/XNwTMqvzeRMZH2w8c5cRVpzpU8Y5bApTppSuUkhZXN0VxHd" crossorigin="anonymous"></script>

    <script type="text/javascript" charset="utf8">
      $(document).ready(function() {

        function updateTotal() {
          var total = $("#subtotalInput").val() * (($("#taxInput").val()/100)+1);
          $("#totalInput").val(Math.floor(total));
        }

        function updateSubtotal() {
          var subtotal = 0;
          if($("#itemBody").children("tr").length <= 0) {$("#btnSave").attr("disabled",true);}
          else {$("#btnSave").removeAttr("disabled");}
          $("#itemBody").children("tr").each(function(i,e) {
            subtotal += parseInt($(e).find(".inputAmount").val());
          });
          $("#subtotalInput").val(Math.floor(subtotal));
          updateTotal();
        }

        function updateRow(row) {
          var price = $(row).find(".inputPrice").val();
          var qty = $(row).find(".inputQty").val();
          $(row).find(".inputAmount").val(qty*price);
          updateSubtotal()
        }
        
        function initActions() {
          $(".btnRemove").click(function() {
            $(this).parents("tr").remove();
            updateSubtotal();
          });

          $(".selectItem").change(function() {
            const option = $(this).children("option:selected");
            var row = $(this).parents("tr");
            $(row).find(".inputType").val(option.attr("item_type"));
            $(row).find(".inputDesc").val(option.attr("item_desc"));
            $(row).find(".inputPrice").val(option.attr("item_price"));
            updateRow(row);
          });

          $(".inputQty").change(function() {
            updateRow($(this).parents("tr"));
          });
        }

        $("#selectCustomer").change(function() {
          const option = $(this).children("option:selected");
          var row = $(this).parents("tr");
          $("#forSubjectInput").val(option.attr("for_subject"));
          $("#forAddressInput").val(option.attr("for_address"));
        });

        $("#btnAdd").click(function() {
          $("#itemBody").append(
            `
            <tr>
              <td>
                <label class="sr-only">Item Type</label>
                <select class="form-control selectItem" required>
                  <option value="" item_type="" item_desc="" item_price="0">Select Item</option>
                  <?php for ($j=0; $j < count($item); $j++) { $t = $item[$j]?>
                  <option value="<?php echo $t->id; ?>" item_type="<?php echo $t->type; ?>" item_desc="<?php echo $t->description; ?>" item_price="<?php echo $t->price; ?>" ><?php echo $t->type."(".$t->description.")"; ?></option>
                  <?php } ?>
                </select>
                <input type="hidden" class="form-control inputType" placeholder="Type" name="type[]">
              </td>
              <td>
                <label class="sr-only">Description</label>
                <input type="text" class="form-control inputDesc" placeholder="Description" name="description[]" readonly required>
              </td>
              <td>
                <label class="sr-only">Quantity</label>
                <input type="number" class="form-control inputQty" placeholder="QTY" name="quantity[]" min=1 step=0.1 required>
              </td>
              <td>
                <label class="sr-only">Price</label>
                <input type="text" class="form-control inputPrice" placeholder="Price" name="price[]" value=0 readonly required>
              </td>
              <td>
                <label class="sr-only">Amount</label>
                <input type="text" class="form-control inputAmount" placeholder="Amount" value=0 readonly required>
              </td>
              <td>
                <button class="btn btn-danger btnRemove" type="button" >Remove</button>
              </td>
            </tr>
            `
          );
          initActions();
          if($("#itemBody").children("tr").length <= 0) {$("#btnSave").attr("disabled",true);}
          else {$("#btnSave").removeAttr("disabled");}
        });

        $("#taxInput").change(function() {
          updateTotal();
        }); 

        initActions();
        $(".selectItem").trigger("change");
        $("#selectCustomer").trigger("change");
        <?php if(!$isEdit) { ?>$("#btnAdd").trigger("click");<?php } ?>

      });
    </script>
  </footer>
</html>