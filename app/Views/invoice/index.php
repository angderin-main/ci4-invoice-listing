<html>
  <head>
    <!-- <script type="text/javascript" src="/bootstrap/js/bootstrap.min.js"></script>
    <link href="/bootstrap/css/bootstrap.min.css" rel="stylesheet"> -->
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@3.4.1/dist/css/bootstrap.min.css" integrity="sha384-HSMxcRTRxnN+Bdg0JdbxYKrThecOKuH5zCYotlSAcp1+c8xmyTe9GYg1l9a69psu" crossorigin="anonymous">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@3.4.1/dist/css/bootstrap-theme.min.css" integrity="sha384-6pzBo3FDv/PJ8r2KRkGHifhEocL+1X2rVCTTkUfGk7/0pbek5mMa1upzvWbrUbOZ" crossorigin="anonymous">

    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.12.1/css/jquery.dataTables.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.12.1/css/dataTables.bootstrap.min.css">
  </head>
  <body>
    <?php echo view("template/navbar") ?>
    <div class="container">
      <div class="row">
        <h1>Invoice List</h1>
      </div>
    </div><!-- /.container -->
    <div class="container">
      <div class="row">
        <div class="pull-right">
          <a href="<?php echo base_url("/invoice/form")?>">
            <button class="btn btn-primary" type="button">NEW INVOICE</button>
          </a>
        </div>
      </div>
      <div class="row">&nbsp;</div>
      <div class="row">
        <table id="invoice_list" class="table table-striped table-bordered">
          <thead>
            <tr>
              <!-- <th style="width: 5%">NO</th> -->
              <th>INVOICE ID</th>
              <th>SUBJECT</th>
              <th>FROM</th>
              <th>FOR</th>
              <th>ISSUE DATE</th>
              <th>DUE DATE</th>
              <th style="width: 5%">STATUS</th>
              <th style="width: 5%">ACTION</th>
            </tr>
          </thead>
          <tbody>

          </tbody>
        </table>
      </div>
      
    </div><!-- /.container -->
  </body>
  <footer>
    <!-- JQuery -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <!-- Latest compiled and minified JavaScript -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@3.4.1/dist/js/bootstrap.min.js" integrity="sha384-aJ21OjlMXNL5UyIl/XNwTMqvzeRMZH2w8c5cRVpzpU8Y5bApTppSuUkhZXN0VxHd" crossorigin="anonymous"></script>

    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.12.1/js/jquery.dataTables.js"></script>

    <script type="text/javascript" charset="utf8">
      $(document).ready(function() {
        var table = $('#invoice_list').DataTable({
          processing: true,
          serverSide: true,
          "ajax": '<?php echo base_url("/invoice/get") ?>',
          columns: [
            // {
            //   searchable: false,
            //   orderable: true,
            //   data: 'id',
            // },
            {
              searchable: true,
              orderable: true,
              data: 'invoice_id',
            },
            {
              searchable: true,
              orderable: true,
              data: 'subject',
            },
            {
              searchable: true,
              orderable: true,
              data: 'from_subject',
            },
            {
              searchable: true,
              orderable: true,
              data: 'for_subject',
            },
            {
              searchable: false,
              orderable: true,
              data: 'issue_date',
            },
            {
              searchable: false,
              orderable: true,
              data: 'due_date'
            },
            {
              searchable: false,
              orderable: false,
              data: 'status',
            },
            {
              searchable: false,
              orderable: false,
              data: null,
              defaultContent: '<button class="btn btn-primary" >DETAIL</button>',
            },
          ],
        });

        $("#invoice_list tbody").on("click", "button", function() {
          console.log($(this).parents("tr"))
          var data = table.row($(this).parents("tr")).data();
          var win = window.open('<?php echo base_url("/invoice/detail") ?>'+"/"+data.invoice_id, '_blank');
          if (win) {
            win.focus();
          } else {
            alert('Please allow popups for this website');
          }
        })
      });
    </script>
  </footer>
</html>