<html>
  <head>
    <!-- <script type="text/javascript" src="/bootstrap/js/bootstrap.min.js"></script>
    <link href="/bootstrap/css/bootstrap.min.css" rel="stylesheet"> -->
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@3.4.1/dist/css/bootstrap.min.css" integrity="sha384-HSMxcRTRxnN+Bdg0JdbxYKrThecOKuH5zCYotlSAcp1+c8xmyTe9GYg1l9a69psu" crossorigin="anonymous">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@3.4.1/dist/css/bootstrap-theme.min.css" integrity="sha384-6pzBo3FDv/PJ8r2KRkGHifhEocL+1X2rVCTTkUfGk7/0pbek5mMa1upzvWbrUbOZ" crossorigin="anonymous">

    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.12.1/css/jquery.dataTables.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.12.1/css/dataTables.bootstrap.min.css">
  </head>
  <body>
    <?php echo view("template/navbar") ?>
    <div class="container">
      <div class="row">
        <h1>Invoice - <?php echo $invoice->invoice_id ?></h1>
      </div>
    </div><!-- /.container -->
    <div class="container">
      <div class="row">
        <div class="col-md-1 pull-right">
          <button class="btn btn-danger btn-block" type="button" id="btnDelete">Delete</button>
          <form method="POST" id="formDelete" action="<?php echo base_url("invoice/delete"); ?>">
            <input type="hidden" name="invoiceId" value="<?php echo $invoice->invoice_id ?>">
          </form>
        </div>
        <div class="col-md-1 pull-right">
          <a href="<?php echo base_url("invoice/form/".$invoice->invoice_id) ?>"><button class="btn btn-primary btn-block" type="button">Edit</button></a>
        </div>
      </div>
      <div class="row">&nbsp;</div>
      <form class="form-horizontal" method="POST" action="<?php echo base_url("invoice/update"); ?>">
        <div class="row">
          <div class="col-md-6 show-grid">
            <div class="form-group">
              <label for="invoiceIdInput" class="col-md-3 control-label">Invoice ID</label>
              <div class="col-md-9">
                <input type="text" class="form-control" id="invoiceIdInput" placeholder="Invoice ID" value="<?php echo $invoice->invoice_id?>" readonly>
              </div>
            </div>
            <div class="form-group">
              <label for="issueDateInput" class="col-md-3 control-label">Issue Date</label>
              <div class="col-md-9">
                <input type="datetime" class="form-control" id="issueDateInput" placeholder="Issue Date" value="<?php echo $invoice->issue_date?>" readonly>
              </div>
            </div>
            <div class="form-group">
              <label for="dueDateInput" class="col-md-3 control-label">Due Date</label>
              <div class="col-md-9">
                <input type="datetime" class="form-control" id="dueDateInput" placeholder="Due Date" value="<?php echo $invoice->due_date?>" readonly>
              </div>
            </div>
            <div class="form-group">
              <label for="subjectInput" class="col-md-3 control-label">Subject</label>
              <div class="col-md-9">
                <input type="text" class="form-control" id="subjectInput" placeholder="Subject" value="<?php echo $invoice->subject?>" readonly>
              </div>
            </div>
          </div>
          <div class="col-md-6 pull-right">
            <div class="form-group">
              <label for="fromSubjectInput" class="col-md-3 control-label">From Subject</label>
              <div class="col-md-9">
                <input type="text" class="form-control" id="fromSubjectInput" placeholder="From Subject" value="<?php echo $invoice->from_subject?>" readonly>
              </div>
            </div>
            <div class="form-group">
              <label for="fromAddressInput" class="col-md-3 control-label">From Address</label>
              <div class="col-md-9">
                <textarea class="form-control" rows="3" id="fromAddressInput" readonly><?php echo $invoice->from_address?></textarea>
              </div>
            </div>
            <div class="form-group">
              <label for="forSubjectInput" class="col-md-3 control-label">For Subject</label>
              <div class="col-md-9">
                <input type="text" class="form-control" id="forSubjectInput" placeholder="For Subject" value="<?php echo $invoice->for_subject?>" readonly>
              </div>
            </div>
            <div class="form-group">
              <label for="forAddressInput" class="col-md-3 control-label">For Address</label>
              <div class="col-md-9">
                <textarea class="form-control" rows="3" id="forAddressInput" name="forAddress" readonly><?php echo $invoice->for_address?></textarea>
              </div>
            </div>
          </div>
        </div>
        <div class="row">&nbsp;</div>
        <div class="row">
          <div class="col-md-12">
            <table class="table table-striped table-bordered" id="invoiceDetail">
              <thead>
                <tr>
                  <td class="col-md-2">
                    Item Type
                  </td>
                  <td class="col-md-5">
                    Description
                  </td>
                  <td class="col-md-1">
                    Quantity
                  </td>
                  <td class="col-md-2">
                    Unit Price
                  </td>
                  <td class="col-md-2">
                    Amount
                  </td>
                </tr>
              </thead>
              <tbody id="itemBody">
                <?php $subtotal = 0; for ($i=0; $i < count($invoice_detail); $i++) { $item = $invoice_detail[$i] ?>
                <tr>
                  <td>
                    <label class="sr-only">Item Type</label>
                    <input type="text" class="form-control inputType" placeholder="Type" value="<?php echo $item->item_type?>" readonly>
                  </td>
                  <td>
                    <label class="sr-only">Description</label>
                    <input type="text" class="form-control inputDesc" placeholder="Description" value="<?php echo $item->item_description?>" readonly>
                  </td>
                  <td>
                    <label class="sr-only">Quantity</label>
                    <input type="number" class="form-control inputQty" placeholder="QTY" value="<?php echo $item->item_quantity?>" readonly>
                  </td>
                  <td>
                    <label class="sr-only">Price</label>
                    <input type="text" class="form-control inputPrice" placeholder="Price" value="<?php echo $item->item_price?>" readonly>
                  </td>
                  <td>
                    <label class="sr-only">Amount</label>
                    <input type="text" class="form-control inputAmount" placeholder="Amount" value="<?php echo $item->item_price * $item->item_quantity?>" readonly>
                  </td>
                </tr>
                <?php $subtotal+= $item->item_price * $item->item_quantity; } ?>
              </tbody>
            </table>
          </div>
        </div>
        <div class="row">&nbsp;</div>
        <div class="row">
          <div class="col-md-offset-8 col-md-4">
            <div class="form-group">
              <label class="col-md-3 control-label">Subtotal</label>
              <div class="col-md-9">
                <input type="number" class="form-control" placeholder="Subtotal" id="subtotalInput" value="<?php echo $subtotal?>" readonly>
              </div>
            </div>
            <div class="form-group">
              <label for="taxInput" class="col-md-3 control-label">Tax</label>
              <div class="col-md-9">
                <input type="text" class="form-control" id="taxInput" placeholder="Tax" value="<?php echo $invoice->tax."%" ?>" readonly>
              </div>
            </div>
            <div class="form-group">
              <label class="col-md-3 control-label">Total</label>
              <div class="col-md-9">
                <input type="number" class="form-control" placeholder="Total" id="totalInput" value="<?php echo $subtotal * (($invoice->tax/100)+1) ?>" readonly>
              </div>
            </div>
            <div class="form-group">
              <label for="paymentInput" class="col-md-3 control-label">Payment</label>
              <div class="col-md-9">
                <input type="number" class="form-control" id="paymentInput" placeholder="Payment" value="<?php echo $invoice->payment ?>" readonly>
              </div>
            </div>
          </div>
        </div>
        <div class="row">
          
        </div>
      </form>
    </div><!-- /.container -->
  </body>
  <footer>
    <!-- JQuery -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <!-- Latest compiled and minified JavaScript -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@3.4.1/dist/js/bootstrap.min.js" integrity="sha384-aJ21OjlMXNL5UyIl/XNwTMqvzeRMZH2w8c5cRVpzpU8Y5bApTppSuUkhZXN0VxHd" crossorigin="anonymous"></script>
    <script type="text/javascript" charset="utf8">
      $(document).ready(function() {
        $("#btnDelete").click(function() {
          if(confirm("Are you sure want to delete this invoice?")){
            $("#formDelete").submit();
          }else{

          }
        })
      })
    </script>
  </footer>
</html>