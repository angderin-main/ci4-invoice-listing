<?php

namespace App\Controllers;

class Invoice extends BaseController
{
  private $db;
  function __construct() {
    $this->db = db_connect();
  }

  function __destruct() {
    $this->db->close();
  }

  public function index()
  {
    // $builder = $this->db->table('invoice');
    // $builder->select('id,invoice_id, subject, from_subject, for_subject, issue_date, due_date, status');
    // $query = $builder->get();
    // var_dump($query->getResult());
    
    return view('invoice/index.php');
  }

  public function get()
  {
    $start = $this->request->getVar("start");
    $length = $this->request->getVar("length");
    $search = $this->request->getVar("search[value]");
    $json = new \stdClass();
    $json->data = [];
    $builder = $this->db->table('invoice');
    $builder->select('id,invoice_id, subject, from_subject, for_subject, issue_date, due_date, status');
    $builder->like("invoice_id",$search,"both",true,true);
    $builder->orLike("subject",$search,"both",true,true);
    $builder->orLike("from_subject",$search,"both",true,true);
    $builder->orLike("for_subject",$search,"both",true,true);
    $json->recordsFiltered = $builder->countAllResults(false);
    $query = $builder->get($length, $start);
    $json->data = $query->getResult();
    $json->recordsTotal = $builder->countAll();

    return $this->response->setJSON($json);
  }

  private function generateId()
  {
    $builder = $this->db->table("invoice");
    $builder->select("invoice_id");
    $builder->orderBy('invoice_id', 'DESC');
    $query = $builder->get(1,0);
    $last_id = (int)$query->getRow()->invoice_id;
    $new_id = sprintf("%04d",$last_id+1);
    return $new_id;
  }

  public function form($invoice_id = null)
  {
    $data = [];

    $builder = $this->db->table('customer');
    $builder->select('*');
    $query = $builder->get();
    $data["customer"] = $query->getResult();

    $builder = $this->db->table('item');
    $builder->select('*');
    $query = $builder->get();
    $data["item"] = $query->getResult();

    if($invoice_id!=null){

      $builder = $this->db->table('invoice');
      $builder->select('*');
      $builder->where('invoice_id', $invoice_id);
      $query = $builder->get();
      $data["invoice"] = $query->getRow();

      $builder = $this->db->table('invoice_detail');
      $builder->select('*');
      $builder->where('invoice_id', $data["invoice"]->id);
      $query = $builder->get();
      $data["invoice_detail"] = $query->getResult();

    }
    
    return view('invoice/form-new.php',$data);
  }

  public function formp()
  {
    $post = $this->request->getPost();
    if(isset($post["invoiceId"])){
      return $this->update($post);
    }else{
      return $this->insert($post);
    }
  }

  private function insert($post)
  {
    $invoice_id = $this->generateId();
    $invoice_detail = [];
    $subtotal = 0;
    
    $type_list = $post["type"];
    $description_list = $post["description"];
    $quantity_list = $post["quantity"];
    $price_list = $post["price"];

    for ($i=0; $i < count($type_list); $i++) { 
      $invoice_detail[] = [
        "invoice_id" => 0,
        "item_type" => $type_list[$i],
        "item_description" => $description_list[$i],
        "item_quantity" => (int)$quantity_list[$i],
        "item_price" => (int)$price_list[$i]
      ];
      $subtotal += (int)$quantity_list[$i] * (int)$price_list[$i];
    }

    $invoice = [
      "invoice_id" => $invoice_id,
      "issue_date" => $post["issueDate"],
      "due_date" => $post["dueDate"],
      "subject" => $post["subject"],
      "from_subject" => $post["fromSubject"],
      "from_address" => $post["fromAddress"],
      "for_subject" => $post["forSubject"],
      "for_address" => $post["forAddress"],
      "subtotal" => $subtotal,
      "tax" => $post["tax"],
      "payment" => $post["payment"],
      "status" => "PAID"
    ];

    $this->db->transStart();

    $builder = $this->db->table('invoice');
    $builder->insert($invoice);

    $invoice_detail = array_map(function ($item){
      $item["invoice_id"] = $this->db->insertID();
      return $item;
    },$invoice_detail);

    $builder = $this->db->table('invoice_detail');
    $builder->insertBatch($invoice_detail);
    
    $this->db->transComplete();
    if ($this->db->transStatus() === false) {
      // generate an error... or use the log_message() function to log your error
    }
    
    return redirect()->to('/invoice/detail/'.$invoice_id);
  }

  private function update($post)
  {
    $invoice_detail = [];
    $subtotal = 0;

    $builder = $this->db->table('invoice');
    $builder->select("id");
    $builder->where('invoice_id',$post['invoiceId']);
    $query = $builder->get();
    $id = $query->getRow()->id;
    
    $type_list = $post["type"];
    $description_list = $post["description"];
    $quantity_list = $post["quantity"];
    $price_list = $post["price"];

    for ($i=0; $i < count($type_list); $i++) { 
      $invoice_detail[] = [
        "invoice_id" => $id,
        "item_type" => $type_list[$i],
        "item_description" => $description_list[$i],
        "item_quantity" => (int)$quantity_list[$i],
        "item_price" => (int)$price_list[$i]
      ];
      $subtotal += (int)$quantity_list[$i] * (int)$price_list[$i];
    }

    $invoice = [
      "issue_date" => $post["issueDate"],
      "due_date" => $post["dueDate"],
      "subject" => $post["subject"],
      "from_subject" => $post["fromSubject"],
      "from_address" => $post["fromAddress"],
      "for_subject" => $post["forSubject"],
      "for_address" => $post["forAddress"],
      "subtotal" => $subtotal,
      "tax" => $post["tax"],
      "payment" => $post["payment"],
      "status" => "PAID"
    ];

    $this->db->transStart();

    $builder = $this->db->table('invoice');
    $builder->set($invoice);
    $builder->where('id',$id);
    $builder->update();

    $builder = $this->db->table('invoice_detail');
    $builder->where('invoice_id', $id);
    $builder->delete();

    $builder = $this->db->table('invoice_detail');
    $builder->insertBatch($invoice_detail);
    
    $this->db->transComplete();
    if ($this->db->transStatus() === false) {
      // generate an error... or use the log_message() function to log your error
    }
    
    return redirect()->to('/invoice/detail/'.$post["invoiceId"]);
  }

  public function delete()
  {
    $post = $this->request->getPost();
    $this->db->transStart();

    $builder = $this->db->table('invoice');
    $builder->where('invoice_id', $post['invoiceId']);
    $builder->delete();

    $this->db->transComplete();
    if ($this->db->transStatus() === false) {
      // generate an error... or use the log_message() function to log your error
    }
    
    return redirect()->to('invoice');
  }

  public function detail($invoice_id)
  {
    $data = [];
    $builder = $this->db->table('invoice');
    $builder->select('*');
    $builder->where('invoice_id', $invoice_id);
    $query = $builder->get();
    $data["invoice"] = $query->getRow();
    $builder = $this->db->table('invoice_detail');
    $builder->select('*');
    $builder->where('invoice_id', $data["invoice"]->id);
    $query = $builder->get();
    $data["invoice_detail"] = $query->getResult();

    return view('invoice/detail.php',$data);
  }
}
